import React, {
  FC,
  createContext,
  Dispatch,
  SetStateAction,
  useState,
  useContext,
  ReactNode,
  ReactElement,
} from "react";

export interface ContentInterface {
  dark: boolean;
}

export interface ContentStateInterface {
  content: ContentInterface;
  setContent: Dispatch<SetStateAction<ContentInterface>>;
}

export interface ContentContextProviderProps {
  defaults?: Partial<ContentInterface>;
  children?: ReactNode;
}

export const defaultContent: ContentInterface = {
  dark: false,
};

export const useContent = (
  overrides?: Partial<ContentInterface>
): ContentStateInterface => {
  const [content, setContent] = useState<ContentInterface>({
    ...defaultContent,
    ...overrides,
  });
  return { content, setContent };
};

const defaultContentState: ContentStateInterface = {
  content: defaultContent,
  setContent: (): void => {},
};

export const ContentContext = createContext<ContentStateInterface>(
  defaultContentState
);

export const useContentContext = (): ContentStateInterface => {
  return useContext(ContentContext);
};

export const ContentContextProvider: FC<ContentContextProviderProps> = ({
  children,
  defaults,
}): ReactElement => {
  const [content, setContent] = useState<ContentInterface>({
    ...defaultContent,
    ...defaults,
  });
  return (
    <ContentContext.Provider value={{ content, setContent }}>
      {children}
    </ContentContext.Provider>
  );
};
