import React, { FC, useContext } from "react";
import {
  Provider,
  teamsDarkTheme,
  teamsTheme,
} from "@fluentui/react-northstar";
import { ContentStateInterface, ContentContext } from "./ThemeContext";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import SignIn from "./components/SignIn";
import Main from "./components/Main";
import ToolbarComponent from "./components/layout/Toolbar";

const App: FC = () => {
  const {
    content: { dark },
  } = useContext<ContentStateInterface>(ContentContext);

  return (
    <Provider theme={!dark ? teamsDarkTheme : teamsTheme}>
      <ToolbarComponent />
      <BrowserRouter>
        <Switch>
          <Route exact={true} path="/signin" component={SignIn} />
          <Route exact={true} path="/" component={Main} />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
