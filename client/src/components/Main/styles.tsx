export const useStyles = (dark: boolean) => ({
  root: {
    height: "calc(100vh - 70px)",
    padding: 64,
    overflow: "auto",
  },
  viewSwitch: {
    marginBottom: 24,
  },
  meetingTitle: {
    fontSize: 26,
    fontFamily: "Segoe UI",
    fontWeight: 800,
  },
  listContainer: {
    maxWidth: 700,
  },
  headerToolbarContainer: {
    display: "flex",
    justifyContent: "space-between",
  },
});
