import React, { FC, useContext, useState } from "react";
import FullCalendar, { CalendarApi, EventInput } from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import listPlugin from "@fullcalendar/list";
import { useStyles } from "./styles";
import { ContentStateInterface, ContentContext } from "../../ThemeContext";
import {
  AddIcon,
  Button,
  Checkbox,
  Dropdown,
  Flex,
  List,
  Popup,
  Text,
} from "@fluentui/react-northstar";
import MeetingList from "./meeting";
import moment from "moment";
import MeetingForm from "../common/MeetingForm";
const Main: FC = (props: any) => {
  const {
    content: { dark },
  } = useContext<ContentStateInterface>(ContentContext);

  const [calendarMode, setCalendarMode] = useState<boolean>(true);
  const [events, setEvents] = useState<any[]>(MeetingList);
  const [values, setValues] = useState<any>({});

  const handleViewChange = () => {
    setCalendarMode(!calendarMode);
  };

  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setValues((prevState: any) => ({ ...prevState, [name]: value }));
  };

  const submitForm = (e: any) => {
    setEvents((prevState) => [
      ...prevState,
      {
        key: events.length,
        ...values,
        headerMedia: values.date,
        date: moment(values.date).format(),
      },
    ]);
  };

  const classes = useStyles(dark);

  const calendarRef: any = React.createRef();

  const handleNavigation = (e: any) => {
    const calendarApi = calendarRef.current.getApi();
    const { name } = e.target;
    switch (name) {
      case "prevYear":
        calendarApi.prevYear();
        break;
      case "prevMonth":
        calendarApi.prev();
        break;
      case "nextMonth":
        calendarApi.next();
        break;
      case "nextYear":
        calendarApi.nextYear();
        break;

      default:
        break;
    }
  };

  const calendarViews = ["Mois", "Liste semaine"];

  const handleCalendarViewChange = (value: any) => {
    const calendarApi = calendarRef.current.getApi();
    switch (value) {
      case "Liste semaine":
        calendarApi.changeView("listWeek");
        break;
      case "Mois":
        calendarApi.changeView("dayGridMonth");
        break;

      default:
        break;
    }
  };

  return (
    <div style={classes.root}>
      <Checkbox
        label="Switch Calendar/Meeting"
        checked={calendarMode}
        onChange={handleViewChange}
        style={classes.viewSwitch}
        toggle
      />

      {calendarMode ? (
        <div>
          <div style={classes.headerToolbarContainer}>
            <div>
              <Button
                title="Année précédent"
                content="<<"
                name="prevYear"
                onClick={handleNavigation}
              />
              <Button
                content="<"
                title="Mois précédent"
                name="prevMonth"
                onClick={handleNavigation}
              />
            </div>
            <Dropdown
              items={calendarViews}
              placeholder="Select your hero"
              checkable
              onChange={(e, selectedOption) => {
                handleCalendarViewChange(selectedOption.value);
              }}
              defaultActiveSelectedIndex={0}
            />
            <div>
              <Button
                content=">"
                title="Mois suivant"
                name="nextMonth"
                onClick={handleNavigation}
              />
              <Button
                content=">>"
                title="Année suivant"
                name="nextYear"
                onClick={handleNavigation}
              />
            </div>
          </div>
          <FullCalendar
            events={events}
            plugins={[dayGridPlugin, listPlugin]}
            initialView="dayGridMonth"
            headerToolbar={false}
            ref={calendarRef}
            {...props}
          />
        </div>
      ) : (
        <div>
          <Flex gap="gap.large" vAlign="center">
            <Text style={classes.meetingTitle} content="Meetings" />
            <Popup
              align={"top"}
              content={
                <MeetingForm
                  handleChange={handleChange}
                  handleSubmit={submitForm}
                />
              }
              pointing
              position={"after"}
              trigger={
                <Button
                  icon={<AddIcon />}
                  iconOnly
                  title="Ajouter un meeting"
                />
              }
            />
          </Flex>
          <div style={classes.listContainer}>
            <List items={events} navigable={true} />
          </div>
        </div>
      )}
    </div>
  );
};

export default Main;
