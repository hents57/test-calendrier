import React, { FC } from "react";
import { Button, Datepicker, Form, Input } from "@fluentui/react-northstar";

interface MeetingFormProps {
  handleChange: (event: any) => void;
  handleSubmit: (event: any) => void;
}
const MeetingForm: FC<MeetingFormProps> = ({ handleChange, handleSubmit }) => {
  const fields = [
    {
      label: "Titre",
      name: "header",
      id: "header",
      key: "header",
      required: true,
      control: {
        as: Input,
        showSuccessIndicator: false,
        onChange: handleChange,
      },
    },
    {
      label: "Description",
      name: "content",
      id: "content",
      key: "content",
      required: true,
      control: {
        as: Input,
        showSuccessIndicator: false,
        onChange: handleChange,
      },
    },
    {
      label: "Date(MM/DD/YYYY)",
      name: "date",
      id: "date",
      key: "date",
      required: true,
      control: {
        as: Input,
        showSuccessIndicator: false,
        onChange: handleChange,
      },
    },
    {
      control: {
        as: Button,
        content: "Submit",
      },
      key: "submit",
    },
  ];
  return <Form fields={fields} onSubmit={handleSubmit} />;
};

export default MeetingForm;
