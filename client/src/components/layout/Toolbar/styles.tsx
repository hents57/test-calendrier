export const useStyles = (dark: boolean) => ({
  root: {
    height: 70,
    borderBottom: `1px solid ${dark ? "black" : "white"}`,
    display: "flex",
    justifyContent: "space-between",
    paddingRight: 16,
    paddingLeft: 16,
  },
  brand: {
    fontFamily: "Roboto",
    fontWeight: 700,
    fontSize: 20,
    marginLeft: 8,
  },
  brandContainer: {
    display: "flex",
    alignItems: "center",
  },
});
