import React, { FC, useContext } from "react";
import { Checkbox, Toolbar, Text } from "@fluentui/react-northstar";
import { ContentStateInterface, ContentContext } from "../../../ThemeContext";
import { useStyles } from "./styles";
import Logo from "../../../assets/img/logo.png";
const ToolbarComponent: FC = () => {
  const {
    content: { dark },
    setContent,
  } = useContext<ContentStateInterface>(ContentContext);

  const classes = useStyles(dark);

  const handleThemeChange = (e: any) => {
    setContent({ dark: !dark });
  };

  return (
    <Toolbar style={classes.root}>
      <div style={classes.brandContainer}>
        <img src={Logo} />
        <Text style={classes.brand} content="Calendrier" />
      </div>

      <Checkbox
        label="Switch Theme"
        checked={dark}
        onChange={handleThemeChange}
        toggle
      />
    </Toolbar>
  );
};

export default ToolbarComponent;
