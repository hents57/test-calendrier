# Calendrier

Application web de planification des réunions.

## Requirements : Docker & Docker Compose & Npm

### 1- Renommer .env.local en .env

### 2- Gestion des container

Calendrier requiers Docker pour pouvoir être lancé

Pull et build des images

### démarrage

```sh
$ docker-compose up -d --build
```

### arrêt (stop)

```sh
$ docker-compose stop
```

### redemarrage

```sh
$ docker-compose restart
```

### arrêt et suppression des containers et volume

```sh
$ docker-compose down --volume
```

### Nom des containers

Tableau qui contient les noms des containers utilisé dans les commandes docker et docker-compose

| Container  | Nom        |
| ---------- | ---------- |
| MySQL      | mysql      |
| PhpMyAdmin | phpmyadmin |

### 3- Démarrage du client (dossier /client)

- npm install
- npm start

### 4- Démarrage du serveur (dossier /server)

- npm install
- npm start
